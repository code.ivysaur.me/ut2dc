# ut2dc

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

A uTorrent bot for an NMDC server.

It announces completed downloads and can add torrents by PM.

Source code included in download.

Tags: nmdc, torrent

## See Also

Original thread: http://forums.apexdc.net/topic/3962-ut2dc/


## Download

- [⬇️ ut2dc-20110502.rar](dist-archive/ut2dc-20110502.rar) *(40.04 KiB)*
- [⬇️ ut2dc-20110229.rar](dist-archive/ut2dc-20110229.rar) *(39.87 KiB)*
- [⬇️ ut2dc-20110228.rar](dist-archive/ut2dc-20110228.rar) *(38.99 KiB)*
- [⬇️ ut2dc-20101013.rar](dist-archive/ut2dc-20101013.rar) *(43.98 KiB)*
